from scipy import integrate
import sys
theta1=0.7
theta2=0.5

ans = sys.maxsize

for r in range(1,100):
    for b in range(1,100):

        def f(x):
            return 1-(1-x**r)**b

        v, err = integrate.quad(f, theta1, 1)
        p1 = 1 - v/(1-theta1)
        x = (p1 <= 0.01)

        v, err = integrate.quad(f, 0, theta2)
        p2 = v/theta2
        y = (p2 <= 0.01)
        
            
        if x and y and b * r < ans:
            ans = r * b
            print('ans','r=',r,'b=',b)