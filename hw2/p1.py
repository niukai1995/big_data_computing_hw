#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 09:27:38 2019

@author: mark
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 08:34:08 2019

@author: mark
"""


import hashlib 
from random import seed
from random import randint
import pickle
import pandas as pd
import sys

from pyspark.sql import SQLContext
try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3
from pyspark.sql import DataFrameReader
from pyspark.sql import SparkSession

# seed random number generator
seed(1)

# generate some integers
random_nums = [randint(0,1000) for i in range(100)]
def hashFamily(i):
    resultSize = 8 # how many bytes we want back 
    maxLen = 20 # how long can our i be (in decimal) 
    salt = str(i).zfill(maxLen)[-maxLen:] 
    def hashMember(x):
        return hashlib.sha1((x + salt).encode('utf-8')).digest()[-resultSize:] 
    return hashMember

hashes = [hashFamily(i) for i in random_nums]
#sc = SparkContext( 'local')
#logFile = "/Users/mark/SAPIENZA/Session3/Big Data Comupting/hw/hw2/soc-test.txt"
logFile = "/Users/mark/SAPIENZA/Session3/Big Data Comupting/hw/hw2/soc-LiveJournal1Adj.txt"

from pyspark import SparkContext
from pyspark.sql import Row
sc = SparkContext()
sqlContext = SQLContext(sc)
spark = SparkSession.builder \
     .master("local") \
     .appName("Word Count") \
     .config("spark.some.config.option", "some-value") \
     .getOrCreate()

#data = pd.read_csv(logFile, sep="\t", header=None)
train = spark.read.csv(path = logFile, header = False,sep='\t').rdd

# To filter the node which doesn't have friends.
def check(h):
    if not(h[0] and h[1]):
        print(h)
    return h[0] and h[1]

def pair_generator(ele):
    id, lists = ele[0],ele[1]
    
    lists = lists.split(',')
#    lists = [int(i) for i in lists]
    output = []
    for i in lists:
        for j in lists:
            if i == j:
                continue
            output.append(((i,j),[id]))
    return output

#minhash_result = sc.textFile(logFile)
minhash_result = train.filter(check)\
.flatMap(pair_generator)\
.reduceByKey(lambda a,b : a + b)

print('collecting data...')

def output_format(x):
    s = ''
    key,val = x
    s = key[0]+','+key[1]+'\t['
    for i in val[:-1]:
        s = s+ i + ',\t'
    s = s+val[-1]+']'
    return s

result = minhash_result.map(output_format)

result.saveAsTextFile('p1_result.txt')

#print('starting to write back result.....')
#with open("p1_result.txt", "x") as f:
#    for key,val in result:
#        id1,id2 = key
#        save_data = id1 + ',' + id2 + '\t' +'['
#        for ele in val[:-1]:
#            save_data = save_data + ele + ',' + '\t'
#        save_data = save_data + val[-1] + ']'
#        f.write(save_data+'\n')
